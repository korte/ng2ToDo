import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { INote } from './../notes/note.model';
import { ICategory } from './../categories/category.model';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { environment } from './../../environments/environment';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';


@Injectable()
export class NotesService {
  private _notes: INote[] = JSON.parse(localStorage.getItem('notes')) || [];
  constructor(private http: HttpClient, private toastr: ToastrService) { }

  getNotes(): Observable<INote[]> {
    return this.http.get<INote[]>(environment.apiUrl + 'note');
  }

  addNote(content: string, color: string, categoryId: number): Observable<INote> {
    return this.http.post<INote>(environment.apiUrl + 'note', {
      categoryId,
      content,
      color
    }).pipe(
      map(note => {
        this.toastr.success('Note was successfully added');
        return note;
      })
    );
  }

  removeNote(noteId: number) {
    return this.http.delete(environment.apiUrl + 'note/' + noteId).pipe(
      map(() => {
        this.toastr.success('Note was successfully deleted');
      })
    );
  }

  updateNoteColor(noteId: number, color: string): Observable<INote> {
    return this.http.patch<INote>(environment.apiUrl + 'note/' + noteId, {
      color
    }).pipe(
      map(note => {
        this.toastr.success('Note color was successfully updated');
        return note;
      })
    );
  }

  updateNoteCategory(noteId: number, categoryId: number): Observable<INote> {
    return this.http.patch<INote>(environment.apiUrl + 'note/' + noteId, {
      categoryId
    }).pipe(
      map(note => {
        this.toastr.success('Note category was successfully updated');
        return note;
      })
    );
  }

  updateNoteContent(noteId: number, content: string): Observable<INote> {
    return this.http.patch<INote>(environment.apiUrl + 'note/' + noteId, {
      content
    }).pipe(
      map(note => {
        this.toastr.success('Note was successfully updated');
        return note;
      })
    );
  }

  emptyNotesCategory(categoryId: number) {
    this._notes.filter(n => n.category.id === categoryId).forEach(n => n.category = { id: null, title: null });
    localStorage.setItem('notes', JSON.stringify(this._notes));
  }

  updateNotesCategory(categoryId: number, categoryTitle: string) {
    this._notes.filter(n => n.category.id === categoryId).forEach(n => n.category.title = categoryTitle);
    localStorage.setItem('notes', JSON.stringify(this._notes));
  }
}
