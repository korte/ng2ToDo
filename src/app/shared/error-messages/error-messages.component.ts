import { Component, OnInit, Input } from '@angular/core';
import { FormControlName, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'error-messages',
  templateUrl: './error-messages.component.html',
  styleUrls: ['./error-messages.component.scss']
})
export class ErrorMessagesComponent implements OnInit {

  @Input() errors: ValidationErrors;
  

  constructor() { }

  ngOnInit() {
  }

 

}
