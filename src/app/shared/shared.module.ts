import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CookieModule } from 'ngx-cookie';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './token-interceptor.service';

import { ColorPickerComponent } from './color-picker/color-picker.component';

import { CategoriesService } from './categories.service';
import { NotesService } from './notes.service';
import { AutoHeightDirective } from './auto-height.directive';
import { ErrorMessagesComponent } from './error-messages/error-messages.component';


@NgModule({
  imports: [CommonModule, FormsModule, HttpClientModule, CookieModule.forChild()],
  declarations: [ColorPickerComponent, AutoHeightDirective, ErrorMessagesComponent],
  exports: [FormsModule, HttpClientModule, CookieModule, ColorPickerComponent, AutoHeightDirective, ErrorMessagesComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [CategoriesService, NotesService]
    };
  }
}
