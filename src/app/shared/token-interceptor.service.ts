import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  private userToken;

  constructor(private cookies: CookieService) {
    this.userToken = JSON.parse(this.cookies.get('user'))['token'];
    console.log(`UserToken: ${this.userToken}`)
  }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    /**Omit adding Authorization header on requests where its not needed */
    if (request.method === 'POST' &&
      ['/token', '/user', '/activate'].filter(endpoint => request.url.includes(endpoint)).length) {
      return next.handle(request);
    };

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.userToken}`
      }
    });

    return next.handle(request);
  }

}
