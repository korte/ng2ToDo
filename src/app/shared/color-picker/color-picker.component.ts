import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {
  @Input() selectedColor: string;
  @Output() colorSelect: EventEmitter<string> = new EventEmitter<string>();
  availableColors: string[];

  constructor() {
    this.availableColors = ['#efefef', '#fb9a39', '#9afb53', '#eac2ca', '#90dcc0', '#ff82b2', '#eef04f'];
   }

  ngOnInit() {
  }

  selectColor(color: string) {
    this.colorSelect.emit(color);
  }

}
