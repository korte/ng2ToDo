import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { NotesService } from './notes.service';
import { ICategory } from './../categories/category.model';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

@Injectable()
export class CategoriesService {
  private _categories: ICategory[];
  constructor(private notesService: NotesService, private http: HttpClient, private toastr: ToastrService) { }

  addCategory(categoryTitle: string): Observable<ICategory> {
    return this.http.post<ICategory>(environment.apiUrl + 'category', {
      title: categoryTitle
    }).pipe((
      map(category => {
        this.toastr.success('Category was successfully added');
        return category;
      })
    ));
  }

  getCategories(): Observable<ICategory[]> {
    return this.http.get<ICategory[]>(environment.apiUrl + 'category');
  }

  removeCategory(categoryId: number) {
    return this.http.delete(environment.apiUrl + 'category/' + categoryId).pipe(
      map(() => {
        this.toastr.success('Category was successfully deleted');
      })
    )
  }

  updateCategory(categoryId: number, categoryTitle: string): Observable<ICategory> {
    return this.http.put<ICategory>(environment.apiUrl + 'category/' + categoryId, {
      title: categoryTitle
    }).pipe(
      map(category => {
        this.toastr.success('Category was successfully updated');
        return category;
      })
    );
  };
}
