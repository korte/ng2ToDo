import { FormControl } from '@angular/forms';

export const matchTo = (controlName: string) => {
    return (formControl: FormControl) => {
        if (!(formControl && formControl.parent)) {
            return;
        };

        const inputControlValue = formControl.value;
        const matchToValue = formControl.parent.get(controlName).value;
        const matched = inputControlValue === matchToValue;
        return matched ? null : {
            notMatched: inputControlValue !== matchToValue,
            matchTo: controlName
        };
    }
};