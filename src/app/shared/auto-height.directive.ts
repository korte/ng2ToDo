import { Directive, OnInit, OnDestroy, ElementRef, Input } from '@angular/core';
import {NgModel} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

@Directive({
  selector: '[appAutoHeight]',
  providers: [NgModel]
})
export class AutoHeightDirective implements OnInit, OnDestroy {
  scrollChangeSubscription: Subscription;
  el: HTMLTextAreaElement;

  constructor(private ref: ElementRef, private ngModel: NgModel) {
    this.el = ref.nativeElement;
    this.scrollChangeSubscription = this.ngModel.valueChanges.subscribe(newVal => this.adjustHeight());
  }

  ngOnInit() {
    this.adjustHeight();
  }

  ngOnDestroy() {
    this.scrollChangeSubscription.unsubscribe();
  }

  adjustHeight() {
    const defaultHeight = 0;
    this.el.style.height = defaultHeight + 'px';
    this.el.style.height = this.el.scrollHeight + 'px';
  }
}
