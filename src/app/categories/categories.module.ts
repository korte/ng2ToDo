import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import { CategoriesComponent } from './categories/categories.component';
import { categoriesRoutes } from './routes';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CategoriesListItemComponent } from './categories-list-item/categories-list-item.component';
import { CategoriesListItemNewComponent } from './categories-list-item-new/categories-list-item-new.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(categoriesRoutes),
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [CategoriesComponent, CategoriesListComponent, CategoriesListItemComponent, CategoriesListItemNewComponent]
})
export class CategoriesModule { }
