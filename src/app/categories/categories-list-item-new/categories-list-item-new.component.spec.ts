import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesListItemNewComponent } from './categories-list-item-new.component';

describe('CategoriesListItemNewComponent', () => {
  let component: CategoriesListItemNewComponent;
  let fixture: ComponentFixture<CategoriesListItemNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesListItemNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesListItemNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
