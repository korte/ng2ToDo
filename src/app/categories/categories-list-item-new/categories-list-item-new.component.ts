import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-categories-list-item-new',
  templateUrl: './categories-list-item-new.component.html',
  styleUrls: ['./categories-list-item-new.component.scss']
})
export class CategoriesListItemNewComponent implements OnInit {
  @Output() add: EventEmitter<string> = new EventEmitter<string>();
  categoryTitle: string;
  
  constructor() { }

  ngOnInit() {
  }

  onCategoryAdd(categoryTitle: string) {
    this.add.emit(categoryTitle);
    this.categoryTitle = null;
  }

}
