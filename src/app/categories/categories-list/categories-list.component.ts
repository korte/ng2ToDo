import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ICategory } from './../category.model';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent {
  @Input() categories: ICategory[];
  @Output() removeCategory: EventEmitter<number> = new EventEmitter<number>();
  @Output() addCategory: EventEmitter<{ categoryTitle: string }> = new EventEmitter<{ categoryTitle: string }>();
  @Output() updateCategory: EventEmitter<{ categoryId: number, categoryTitle: string }> =
    new EventEmitter<{ categoryId: number, categoryTitle: string }>();

  constructor() {
  }

  add(categoryTitle: string) {
    this.addCategory.emit({ categoryTitle });
  }

  remove(categoryId) {
    this.removeCategory.emit(categoryId);
  }

  update(categoryTitle, categoryId) {
    this.updateCategory.emit({ categoryId, categoryTitle });
  }

}
