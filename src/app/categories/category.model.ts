import { FormControl } from '@angular/forms';
export interface ICategory {
    id: number;
    title: string;
}
