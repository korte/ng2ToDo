import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './../../shared/categories.service';
import { ICategory } from './../category.model';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categories: ICategory[] = [];

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
    this.categoriesService.getCategories().subscribe(categories => this.categories = categories);
  }

  removeCategory(categoryId) {
    this.categoriesService.removeCategory(categoryId).subscribe(() => this.categories.splice(this.categories.findIndex(c => c.id === categoryId), 1));
  }

  addCategory({ categoryTitle }) {
    this.categoriesService.addCategory(categoryTitle).subscribe(category => this.categories.push(category));
  }

  updateCategory({ categoryId, categoryTitle }) {
    this.categoriesService.updateCategory(categoryId, categoryTitle).subscribe(updatedCategory => { this.categories[this.categories.findIndex(c => c.id === categoryId)] = updatedCategory });
  };
}
