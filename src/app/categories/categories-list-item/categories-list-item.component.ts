import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms'
import { ICategory } from './../category.model';
import { debounceTime, distinctUntilChanged, switchMap, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-categories-list-item',
  templateUrl: './categories-list-item.component.html',
  styleUrls: ['./categories-list-item.component.scss']
})
export class CategoriesListItemComponent implements OnInit, OnDestroy {
  @Input() category: ICategory;
  @Output() remove = new EventEmitter();
  @Output() update: EventEmitter<string> = new EventEmitter<string>();
  categoryControl: FormControl;
  destroyed = new Subject();

  constructor() { }

  ngOnInit() {
    this.addFormControl();
  }

  ngOnDestroy() {
    this.destroyed.next();
  }

  addFormControl() {
    this.categoryControl = new FormControl();
    this.categoryControl.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      takeUntil(this.destroyed)
    ).subscribe(newVal => {
      this.update.emit(newVal);
    })
  }

  onRemove() {
    this.remove.emit();
  }

}
