import {Routes} from '@angular/router';
import {CategoriesComponent } from './categories/categories.component';

export const categoriesRoutes: Routes = [
    {path: '', component: CategoriesComponent}
];
