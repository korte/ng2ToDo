import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { filter } from 'rxjs/operators/filter';
import { map } from 'rxjs/operators/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'app';
  subscriptions: Subscription[] = [];
  outAppStates: string[] = [
                            'auth/login',
                            'auth/signup',
                            'auth/activate'
                          ];
  inApp: boolean = false;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.subscriptions.push(
      this.router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .pipe(map(() => this.router.url))
      .subscribe((currentUrl) => {
        this.updateInApp(currentUrl);
      })
    )
  }

  ngOnDestroy(){
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  updateInApp(currentUrl: string) {
    this.inApp = !this.outAppStates.find(state => currentUrl.includes(state));
  }
}
