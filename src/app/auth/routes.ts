import { Routes } from '@angular/router';

import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ActivateComponent } from './activate/activate.component';

export const authRoutes: Routes = [
    {
        path: '', component: AuthComponent, pathMatch:'prefix', children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'login', component: LoginComponent },
            { path: 'signup', component: SignupComponent },
            { path: 'activate/:token', component: ActivateComponent }
        ],
    }
];
