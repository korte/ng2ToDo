import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { map } from 'rxjs/operators/map';
import { catchError } from 'rxjs/operators/catchError';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class AuthService {

  private readonly apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private toastr: ToastrService, private cookies: CookieService) { }

  login(username, password) {
    const emailRegex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const body = {
      password
    };

    const isEmail = emailRegex.test(username);

    Object.assign(body, isEmail ? {
      email: username
    } : {
        username
      });

    return this.http.post(this.apiUrl + 'token', body).pipe(
      map(response => {
        this.cookies.putObject('user', response);
        return Observable.of(true);
      })
    ).pipe(
      catchError(e => {
        if (e.status === 401) {
          this.toastr.error('Invalid username or password');
          return Observable.of(false);
        };
        if(e.status === 403) {
          this.toastr.error(e.error.message);
          return Observable.of(false);
        };
        this.toastr.error('There was an unexpected error')
        return Observable.of(false);
      })
    )
  }

  signUp(username, email, password, activationUrl) {
    return this.http.post(this.apiUrl + 'user', {
      username, email, password, activationUrl
    }).pipe(map(response => {
      return Observable.of(true);
    }))
      .pipe(catchError(e => {
        if (e.error.errors && e.error.errors.length) {
          e.error.errors.forEach(e => this.toastr.error(e.message));
          return Observable.of(false);
        };
        return Observable.of(false);
      }))
  }

  activate(token) {
    return this.http.post(this.apiUrl + 'user/activate', { token })
      .pipe(map(response => {
        this.cookies.putObject('user', response);
        return Observable.of(true);
      }))
      .pipe(catchError(err => Observable.of(false)));
  }

}
