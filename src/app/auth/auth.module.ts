import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { authRoutes } from './routes';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';

import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { SignupComponent } from './signup/signup.component';
import { AuthComponent } from './auth/auth.component';
import { ActivateComponent } from './activate/activate.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(authRoutes)
  ],
  declarations: [LoginComponent, SignupComponent, AuthComponent, ActivateComponent],
  providers: [AuthService]
})
export class AuthModule { }
