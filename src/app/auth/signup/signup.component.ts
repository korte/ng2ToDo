import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { matchTo } from './../../shared/match-to.validator';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  signedUp: boolean = false;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'username': new FormControl('', [Validators.required, Validators.minLength(8)]),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'passwordConfirm': new FormControl('', [matchTo('password')])
    });
  };

  signUp() {
    if (this.signupForm.invalid) {
      return;
    };

    const { username, password, email } = this.signupForm.value;

    this.auth.signUp(username, email, password, `${location.host}/auth/activate`).subscribe(response => {
      this.signedUp = !!response;
    });
  }

}
