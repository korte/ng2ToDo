import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.scss']
})
export class ActivateComponent implements OnInit, OnDestroy {

  constructor(private activatedRoute: ActivatedRoute, private auth: AuthService) { }
  private subscription: Subscription;
  activated: boolean = false;

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      if (!params['token']) return;
      this.auth.activate(params['token']).subscribe(token => {
        if (!token) return;
        this.activated = true;
      })
    });
  };

  ngOnDestroy() {
    this.subscription.unsubscribe();
  };

}
