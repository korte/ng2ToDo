import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { INote } from './../note.model';
import { ICategory } from './../../categories/category.model';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {
  @Input() availableCategories: ICategory[];
  @Output()
  add = new EventEmitter<{
    content: string;
    color: string;
    categoryId: number;
  }>();
  note: INote;
  category = '';
  color: string;
  private readonly defaultColor = '#efefef';

  constructor() {}

  ngOnInit() {
    this.color = this.defaultColor;
  }

  addNote(addNoteForm: NgForm) {
    const { content, category } = addNoteForm.value;
    this.add.emit({ content, color: this.color, categoryId: category });
    addNoteForm.reset();
    this.category = '';
    this.color = this.defaultColor;
  }

  setColor(color: string) {
    this.color = color;
  }
}
