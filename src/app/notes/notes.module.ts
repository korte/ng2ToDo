import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './../shared/shared.module';

import { notesRoutes } from './routes';
import { NotesComponent } from './notes/notes.component';
import { AddNoteComponent } from './add-note/add-note.component';
import { SingleNoteComponent } from './single-note/single-note.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(notesRoutes),
    NgbModule,
    SharedModule
  ],
  declarations: [NotesComponent, AddNoteComponent, SingleNoteComponent]
})
export class NotesModule { }
