import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { INote } from './../note.model';
import { ICategory } from './../../categories/category.model';

@Component({
  selector: 'app-single-note',
  templateUrl: './single-note.component.html',
  styleUrls: ['./single-note.component.scss']
})
export class SingleNoteComponent implements OnInit {
  @Input() note: INote;
  @Input() availableCategories: ICategory[];
  @Output() remove = new EventEmitter();
  @Output() updateContent: EventEmitter<string> = new EventEmitter<string>();
  @Output() updateColor: EventEmitter<string> = new EventEmitter<string>();
  @Output() updateCategory: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  removeNote() {
    this.remove.emit();
  }

  onCategoryUpdate(categoryId) {
    this.updateCategory.emit(categoryId);
  }

  onContentUpdate() {
    this.updateContent.emit(this.note.content);
  }

  onColorUpdate(color) {
    this.updateColor.emit(color);
  }

}
