import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './../../shared/categories.service';
import { NotesService } from './../../shared/notes.service';
import { ICategory } from './../../categories/category.model';
import { INote } from '../note.model';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {
  categories: ICategory[];
  notes: INote[] = [];

  constructor(
    private categoriesService: CategoriesService,
    private notesService: NotesService
  ) { }

  ngOnInit() {
    this.categoriesService.getCategories().subscribe(categories => this.categories = categories);
    this.notesService.getNotes().subscribe(notes => this.notes = notes);
  }

  addNote(note) {
    const { content, color, categoryId } = note;
    this.notesService.addNote(content, color, categoryId).subscribe(note => this.notes.push(note));
  }

  removeNote(noteId: number) {
    this.notesService.removeNote(noteId).subscribe(() => this.notes.splice(this.notes.findIndex(note => note.id === noteId), 1));
  }

  updateColor(color, noteId) {
    this.notesService.updateNoteColor(noteId, color).subscribe(updatedNote => this.notes[this.notes.findIndex(note => note.id === noteId)] = updatedNote);
  }

  updateContent(content, noteId) {
    this.notesService.updateNoteContent(noteId, content).subscribe(updatedNote => this.notes[this.notes.findIndex(note => note.id === noteId)] = updatedNote);
  }

  updateCategory(categoryId, noteId) {
    this.notesService.updateNoteCategory(noteId, categoryId).subscribe(updatedNote => this.notes[this.notes.findIndex(note => note.id === noteId)] = updatedNote);
  }


}
