import { ICategory } from './../categories/category.model';
export interface INote {
    id: number;
    content: string;
    color: string;
    category: ICategory;
}
