import { Routes } from '@angular/router';
import { NotesComponent } from './notes/notes/notes.component';
import { CategoriesComponent } from './categories/categories/categories.component';

export const appRoutes: Routes = [
    {path: '', redirectTo: 'auth', pathMatch: 'full'},
    {path: 'auth', loadChildren: './auth/auth.module#AuthModule'},
    // {path: 'signup', loadChildren: './auth/auth.module#AuthModule'},
    {path: 'notes', loadChildren: './notes/notes.module#NotesModule'},
    {path: 'categories', loadChildren: './categories/categories.module#CategoriesModule'},
];
