import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NavComponent } from './nav/nav.component';
import { NavbarComponent } from './nav/navbar/navbar.component';

@NgModule({
  imports: [CommonModule, RouterModule, NgbModule],
  declarations: [NavComponent, NavbarComponent],
  exports: [NavComponent]
})
export class NavigationModule {}
